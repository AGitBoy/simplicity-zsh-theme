# simplicity zsh theme
A simple, ASCII-only prompt for zsh, with error code indication, user context (shows 
hostname  depending on your SSH or root status), working directory, git status, 
and a prompt character that doubles as a vi mode indicator.

