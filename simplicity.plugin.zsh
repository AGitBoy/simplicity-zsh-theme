# Setup prompt
unset PROMPT
unset RPROMPT

setopt PROMPT_SUBST

# Error code segment
PROMPT='%(?..%B%F{red}(%?%)%f%b )'

# Context
# Shows hostname if using SSH or logged in as root
if [[ -n "${SSH_CONNECTION-}${SSH_CLIENT-}${SSH_TTY-}" ]] || (( EUID == 0 )); then
  psvar[1]="$(print -P '@%m')"
else
  psvar[1]=''
fi

PROMPT+='%B%F{%(!.red.green)}%n%1v%b%f '

# Working directory
PROMPT+='%B%F{blue}%~%b%f '

# Git status
prompt_git_status() {
  # taken from https://github.com/agkozak/agkozak-zsh-prompt
  local ref branch
  ref=$(command git symbolic-ref --quiet HEAD 2> /dev/null)
  case $? in
    0) ;;
    128) return ;;
    *) ref=$(command git rev-parse --short HEAD 2> /dev/null) || return ;;
  esac
  branch=${ref#refs/heads/}

  if [[ -n $branch ]]; then
    local git_status symbols char
    git_status="$(LC_ALL=C command git status 2>&1)"

    typeset -A messages
    messages=(
      '&*'  ' have diverged,'
      '&'   'Your branch is behind '
      '*'   'Your branch is ahead of '
      '+'   'new file:   '
      'x'   'deleted:    '
      '!'   'modified:   '
      '>'   'renamed:    '
      '?'   'Untracked files:'
    )

    for char in '&*' '&' '*' '+' 'x' '!' '>' '?'; do
      case $git_status in
        *${messages[$char]}*) symbols+="$char" ;;
      esac
    done

    [[ -n $symbols ]] && symbols=" ${symbols}"

    printf '(%s%s) ' "$branch" "$symbols"
  fi
}

PROMPT+='%F{yellow}$(prompt_git_status)%f'

# Prompt character
# Changes to '>' when in vi command mode
function zle-line-init zle-keymap-select {
  [[ $KEYMAP = 'vicmd' ]] && psvar[2]='vicmd' || psvar[2]=''
  zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

PROMPT+='%(2V.>.%#) '
